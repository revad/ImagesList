package com.example.imageslist;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.example.imageslist.adapter.ItemAdapter;
import com.example.imageslist.data.ImageViewModel;
import com.example.imageslist.model.ImageItem;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements ItemAdapter.ListItemClickListener {

    private ItemAdapter mAdapter;
    private ProgressBar spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView mItemsList;
        ArrayList<ImageItem> imageItemsList;
        ImageViewModel imageViewModel;
        TextView emptyStateView;
        spinner = findViewById(R.id.spinner_widget);
        mItemsList = findViewById(R.id.recycler_view);
        emptyStateView = findViewById(R.id.empty_state_text_view);


        if (isNetworkAvailable()) {
            spinner.setVisibility(View.VISIBLE);
            emptyStateView.setVisibility(View.GONE);
            GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
            mItemsList.setLayoutManager(layoutManager);
            mItemsList.setHasFixedSize(true);
            imageItemsList = new ArrayList<ImageItem>();
            mAdapter = new ItemAdapter(this, imageItemsList);
            mItemsList.setAdapter(mAdapter);

            imageViewModel = new ViewModelProvider(this).get(ImageViewModel.class);
            imageViewModel.getImageItemsList().observe(this, imageList -> {
                spinner.setVisibility(View.GONE);
                mAdapter.setData(imageList);
            });
        }
        else {
            spinner.setVisibility(View.GONE);
            emptyStateView.setVisibility(View.VISIBLE);
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    @Override
    public void onListItemClick(ImageItem currentImageItem) {
        Intent imageEditIntent;
        imageEditIntent = new Intent(MainActivity.this, ImageEditActivity.class);
        imageEditIntent.putExtra("IMAGE_DATA", currentImageItem.getUrl());
        startActivity(imageEditIntent);
    }

}