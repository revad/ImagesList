package com.example.imageslist.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ImageItem implements Parcelable {

    private final String mUrl;

    public ImageItem(String url) {
        mUrl = url;
    }

    protected ImageItem(Parcel in) {
        mUrl = in.readString();
    }

    public static final Creator<ImageItem> CREATOR = new Creator<ImageItem>() {
        @Override
        public ImageItem createFromParcel(Parcel in) {
            return new ImageItem(in);
        }

        @Override
        public ImageItem[] newArray(int size) {
            return new ImageItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mUrl);
    }

    public String getUrl() {
        return mUrl;
    }
}
