package com.example.imageslist;

import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import androidx.appcompat.app.AppCompatActivity;
import com.example.imageslist.data.FetchDataForUpload;
import com.example.imageslist.data.SendImage;
import com.squareup.picasso.Picasso;

public class ImageEditActivity extends AppCompatActivity {

    private ImageView imageView;
    private String uploadUrl;
    private String imageUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_edit);
        imageView = findViewById(R.id.image_view);
        imageUrl = getIntent().getExtras().getString("IMAGE_DATA");

        Picasso.get()
                .load(imageUrl)
                .placeholder(R.drawable.image_placeholder)
                .error(R.drawable.error_placeholder)
                .into(imageView);

        new LoadData().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.image_edit_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_save) {
            try {
                new SendImage( this,
                        ((BitmapDrawable)imageView.getDrawable()).getBitmap(),
                        uploadUrl,
                        imageUrl
                );
            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class LoadData extends AsyncTask<Void, Void, String> {

        public LoadData() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(Void... params) {
            return FetchDataForUpload.fetchUploadUrl();
        }

        protected void onPostExecute(String result) {
            uploadUrl = result;
        }
    }
}