package com.example.imageslist.adapter;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.example.imageslist.R;
import com.example.imageslist.model.ImageItem;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemViewHolder> {

    private final ArrayList<ImageItem> mImageItemsData;
    final private ListItemClickListener mOnClickListener;

    public interface ListItemClickListener {
        void onListItemClick(ImageItem imageItemData);
    }

    public ItemAdapter(ListItemClickListener listener, ArrayList<ImageItem> imageItemsData) {
        mOnClickListener = listener;
        mImageItemsData = new ArrayList<ImageItem>();
        mImageItemsData.addAll(imageItemsData);
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();
        int layoutIdForListItem = R.layout.image_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(layoutIdForListItem, viewGroup, false);
        ItemViewHolder viewHolder = new ItemViewHolder(view);

        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        int displayWidth = metrics.widthPixels;
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.width = (int) displayWidth /2;
        params.height = (int) (params.width * 1.5);
        view.setLayoutParams(params);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        Picasso.get()
                .load(mImageItemsData.get(position).getUrl())
                .placeholder(R.drawable.image_placeholder)
                .error(R.drawable.error_placeholder)
                .into(holder.gridItemView);
    }

    public void setData(ArrayList<ImageItem> imageItemList) {
        mImageItemsData.addAll(imageItemList);
        update();
    }

    @Override
    public int getItemCount() {
        return mImageItemsData.size();
    }

    public ImageItem getImageItemData(int item) {
        return mImageItemsData.get(item);
    }

    public void update() { notifyDataSetChanged(); }

    class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final ImageView gridItemView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            gridItemView = itemView.findViewById(R.id.imageItemView);
            gridItemView.setScaleType(ImageView.ScaleType.FIT_XY);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int clickedPosition = getAbsoluteAdapterPosition();
            mOnClickListener.onListItemClick(getImageItemData(clickedPosition));
        }
    }
}
