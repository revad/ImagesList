package com.example.imageslist.data;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import com.example.imageslist.model.ImageItem;
import java.util.ArrayList;

public class ImageViewModel extends AndroidViewModel {

    @Nullable
    private JsonLiveData imageItemsList;

    public ImageViewModel (@NonNull Application application) {
        super(application);
        if(imageItemsList == null) {
            imageItemsList = new JsonLiveData(application);
        }
    }

    public MutableLiveData<ArrayList<ImageItem>> getImageItemsList() {
        return imageItemsList;
    }
}
