package com.example.imageslist.data;

import android.content.Context;
import android.util.Log;
import androidx.lifecycle.MutableLiveData;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.imageslist.model.ImageItem;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

public class JsonLiveData extends MutableLiveData<ArrayList<ImageItem>> {
    private final Context context;
    private static final String IMAGES_REQUEST_URL = "https://eulerity-hackathon.appspot.com/image";

    public JsonLiveData(Context context){
        this.context=context;
        LoadData();
    }

    private void LoadData() {
        final RequestQueue requestQueue = Volley.newRequestQueue(context);

        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, IMAGES_REQUEST_URL, null,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        ArrayList<ImageItem> imageItemList = new ArrayList<>();

                        for(int i=0; i < response.length(); i++) {
                            try {
                                JSONObject obj=response.getJSONObject(i);
                                String mUrl = obj.optString("url", "Not specified");
                                imageItemList.add(new ImageItem(mUrl));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        setValue(imageItemList);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("VolleyError", error.getMessage());
                    }
                }
        );
        requestQueue.add(getRequest);
    }
}
